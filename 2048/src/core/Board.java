package core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Board {
	private final int[][] board;
	private final int score;
	
	public Board(int size) {
		this.board = new int[size][];
		this.score = 0;
		
		for (int i = 0; i <size; i++) {
			this.board[i] = new int[size];
			for (int j = 0; j<size; j++) {
				board[i][j] = 0;
			}
		}
	}
	
	private Board(int[][] board, int score) {
		this.score = score;
		this.board = new int[board.length][];
		
		for (int i = 0; i <board.length; i++) {
			this.board[i] = Arrays.copyOf(board[i], board[i].length);
		}
	}
	
	public Board placeTile(Cell cell, int number) {
		if(!isEmpty(cell)) {
			throw new IllegalArgumentException(" The cell is not empty !");
		}
		
		Board result = new Board(this.board, this.score);
		result.board[cell.getX()][cell.getY()] = number;
		return result;
	}
	
	private static int[][] transpose(int[][] input){
		int[][] result = new int[input.length][];
		
		for (int i=0; i < input.length; i++) {
			result[i] = new int[input[0].length];
			for (int j=0; j <input.length; j++) {
				result[i][j] = input[j][i];
			}
		}
		
		return result;
	}
	
	private static int[][] reverse(int[][] input){
		int[][] result = new int[input.length][];
		
		for(int i = 0; i < input.length; i++) {
			result[i] = new int[input[0].length];
			for(int j = 0; j < input.length; j++) {
				result[i][j] = input[i][input.length -j -1];
			}
		}
		return result;
	}
	
	public Board move(Move move) {

		
		int[][] tiles = new int[this.board.length][];
		for(int i = 0; i < this.board.length; i++) {
			tiles[i] = Arrays.copyOf(this.board[i], this.board[i].length);
		}
		
		if (move == Move.LEFT || move == Move.RIGHT) {
			tiles = transpose(tiles);
		}
		if (move == Move.LEFT || move == Move.RIGHT) {
			tiles = transpose(tiles);
		}
		
		int[][] result = new int[tiles.length][];
		int newScore = 0;
		
		for (int x = 0; x < tiles.length; ++x) {
		    LinkedList<Integer> thisRow = new LinkedList<>();
		    for (int y = 0; y < tiles[0].length; ++y) {
		        if (tiles[x][y] > 0) {
		            thisRow.add(tiles[x][y]);
		        }
		    }
		    
		    LinkedList<Integer> newRow = new LinkedList<>();
		    while (thisRow.size() >= 2) {
		        int first = thisRow.pop();
		        int second = thisRow.peek();
		        if (second == first) {
		            int newNumber = first * 2;
		            newRow.add(newNumber);
		            newScore += newNumber;
		            thisRow.pop();
		        } else {
		            newRow.add(first);
		        }
		    }
		    newRow.addAll(thisRow);
		    result[x] = new int[tiles[0].length];
		    for (int y = 0; y < tiles[0].length; ++y) {
		        if (newRow.isEmpty()) {
		            result[x][y] = 0;
		        } else {
		            result[x][y] = newRow.pop();
		        }
		    }
		}
		
		if (move == Move.DOWN || move == Move.RIGHT) {
		    result = reverse(result);
		}
		if (move == Move.LEFT || move == Move.RIGHT) {
		    result = transpose(result);
		}
		
		return new Board(result, this.score + newScore);
		
	}
	
	public int getSize() {
		return this.board.length;
	}
	
	public int getScore() {
		return this.score;
	}
	
	public int getCell(Cell cell) {
		return board[cell.getX()][cell.getY()];
	}
	
	public boolean isEmpty(Cell cell) {
		return getCell(cell) == 0;
	}
	
	public List<Cell> emptyCells(){
		List<Cell> result = new ArrayList<>();
		for (int i = 0; i <board.length; i++) {
			for (int j = 0; j<board[i].length; j++) {
				Cell cell = new Cell(i,j);
				if (isEmpty(cell)) {
					result.add(cell);
				}
			}
		}
		return result;
	}

}
