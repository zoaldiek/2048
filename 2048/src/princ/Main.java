package princ;

import core.Board;
import core.Computer;
import core.Human;

public class Main {

	public static void main(String[] args) {
		Board board = new Board(4);
		Computer computer = new Computer();
		Human human = new Human();
		for (int i = 0; i < 2; ++i) {
		    board = computer.makeMove(board);
		}
		
		Human.printBoard(board);
		do {
		    System.out.println("Human move");
		    System.out.println("==========");
		    board = human.makeMove(board);
		    Human.printBoard(board);
		 
		    System.out.println("Computer move");
		    System.out.println("=============");
		    board = computer.makeMove(board);
		    Human.printBoard(board);
		} while (!board.emptyCells().isEmpty());
		 
		System.out.println("Final Score: " + board.getScore());


	}

}
